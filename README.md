### **Get started** ###

1. npm install -g gulp
2. npm install
3. gulp build
4. run index.html

### **Run tests** ###

npm test


### **Change board** ###

**in js/app.jsx**

```
#!javascript

 <Game rows="6" cols="6" cardWidth="160" cardHeight="232" margin="20" /> //rows and cols max 6
```