'use strict';

jest.unmock('../js/components/Counters.jsx');

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Counters from '../js/components/Counters.jsx';

describe('Counters', () => {

    it('click on pause and change button text', () => {
        var component =  <Counters steps={0} found={0} pauseAction={function(){}}/>;
        var DOM = TestUtils.renderIntoDocument(component);

        const node = ReactDOM.findDOMNode(DOM);
        const button = node.querySelector(".pause");

        expect(button.textContent).toEqual('Пауза');

        TestUtils.Simulate.click(button);

        expect(button.textContent).toEqual('Старт');
    });

});