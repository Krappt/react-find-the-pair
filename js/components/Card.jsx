import React from 'react';

export default class Game extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div className={'card '+this.props.status+' '+this.props.found} style={this.props.cardStyle}>
                <div className="front"></div>
                <div className="back" style={{ backgroundImage: 'url(' + this.props.image + ')'}}></div>
            </div>
        );
    }
}