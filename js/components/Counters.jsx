import React from 'react';

export default class Counters extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            gameMinutes: '00',
            gameSeconds: '00',
            pause: false
        };
    }

    componentWillMount() {
        this.showTime();
    }

    componentWillReceiveProps(event) {

    }

    pad ( val ) {
        return val > 9 ? val : "0" + val;
    }

    showTime() {
        var me = this;
        var sec = 0;
        setInterval(()=>{
            if(!me.state.pause) me.setState({gameSeconds: me.pad(++sec%60), gameMinutes: me.pad(parseInt(sec/60,10))});
        }, 1000);
    }

    pauseGame  = () =>  {
        var me = this;
        me.setState({pause: !me.state.pause});
        me.props.pauseAction();
    };

    render() {
        return (
            <div className='counters'>
                <div className='gameTime'>
                    <span>Время игры: </span>
                    <span>{this.state.gameMinutes}</span><span>:</span><span>{this.state.gameSeconds}</span>
                </div>
                <div className='steps'>
                    <span>Количество ходов: </span>
                    <span>{this.props.steps}</span>
                </div>
                <div className='found'>
                    <span>Найдено пар: </span>
                    <span>{this.props.found}</span>
                </div>
                <button className='pause' onClick={this.pauseGame}>
                    <span>
                     {(() => {
                         switch (this.state.pause) {
                             case true: return "Старт";
                             case false: return "Пауза";
                         }
                     })()}
                    </span>
                </button>
            </div>
        );
    }
}