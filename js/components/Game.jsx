import React from 'react';
import Card from './Card';
import Counters from './Counters';
import Lodash from 'lodash';

export default class Game extends React.Component {
    constructor(props) {
        super(props);
        this.wait = false;
        this.state = {
            cards: [],
            rows: (parseInt(props.rows) > 6)?6:parseInt(props.rows),
            cols: (parseInt(props.cols) > 6)?6:parseInt(props.cols),
            cardWidth: parseInt(props.cardWidth),
            cardHeight: props.cardHeight,
            margin: parseInt(props.margin),
            selectedCard: null,
            steps: 0,
            found: 0,
            pause: false
        };
    }

    componentWillMount() {
        this.createCards();
    }

    createCards() {
        var cards = [];
        var cards2;
        var num = (this.state.rows * this.state.cols)/2;
        for (var i = 0; i < num; i++) {
            var item = {};
            var name = "card"+i;

            item.name = name;
            item.image = "images/" + name + ".svg";
            item.found = "";
            item.status = "";

            cards.push(item);
        }

        cards2 = Lodash.map(cards, Lodash.clone);

        cards = Lodash.shuffle(cards.concat(cards2));

        this.setState({cards: cards});
    }

    chooseCard  = (i = 0) =>  {
        var me = this;
        var cards = me.state.cards;
        var card = cards[i];
        var selectedCard = me.state.selectedCard;
        if(!card.found && !card.status && !me.wait && !me.state.pause) {
            card.status = "flipped";
            if(selectedCard && selectedCard.name == card.name) {
                me.wait = true;
                me.setState({steps: ++me.state.steps, found: ++me.state.found});
                setTimeout(()=>{
                    card.found = "found";
                    selectedCard.found = "found";
                    me.setState({selectedCard: null});
                    this.setState({cards: cards});
                    me.wait = false;
                },1500);
            }
            else if (!selectedCard) {
                me.setState({selectedCard: card});
            }
            else if(selectedCard && selectedCard.name != card.name) {
                me.wait = true;
                me.setState({steps: ++me.state.steps});
                setTimeout(()=>{
                    card.status = '';
                    selectedCard.status = '';
                    this.setState({cards: cards});
                    me.setState({selectedCard: null});
                    me.wait = false;
                },1500);
            }

            me.setState({cards: cards});
        }
    };

    pauseGame  = () =>  {
        var me = this;
        me.setState({pause: !me.state.pause});
    };

    render() {
        var cardStyle = {
            width: this.state.cardWidth,
            height: this.state.cardHeight,
            marginRight: this.state.margin,
            marginBottom: this.state.margin
        };

        return (
            <div className="wrap" style={{width: (this.state.cardWidth+this.state.margin)*this.state.cols}}>
                <Counters steps={this.state.steps} found={this.state.found} pauseAction={this.pauseGame}/>
                <div className="board" style={this.state.pause ? {opacity: 0.5} : {}}>
                    {this.state.cards.map(
                        (card,i) => <div className="cardWrap" key={i} onClick={Lodash.partial(this.chooseCard,i)}>
                            <Card cardStyle={cardStyle} name={card.name} image={card.image} found={card.found} status={card.status}/>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}