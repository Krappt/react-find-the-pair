import ReactDOM from 'react-dom';
import React from 'react';
import Game from './components/Game';

/*
 rows <= 6
 cols <= 6
 */

ReactDOM.render(
    <Game rows="6" cols="6" cardWidth="160" cardHeight="232" margin="20" />,
    document.querySelector('.root')
);