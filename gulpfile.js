var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var eslint = require('gulp-eslint');
var jsFiles = {
    source: [
        'js/Card.jsx',
        'js/Counters.jsx',
        'js/Game.jsx'
    ]
};

gulp.task('eslint', function() {
    return gulp.src(jsFiles.source)
        .pipe(eslint({
            baseConfig: {
                "ecmaFeatures": {
                    "jsx": true
                }
            }
        }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('build', ['eslint'], function () {
    return browserify({entries: './js/app.jsx', extensions: ['.jsx'], debug: true})
        .transform('babelify', {presets: ['es2015', 'react', 'stage-0']})
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('out'));
});

gulp.task('watch', ['build'], function () {
    gulp.watch('*.jsx', ['build']);
});

gulp.task('default', ['watch']);